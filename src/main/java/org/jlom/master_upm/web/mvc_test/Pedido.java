package org.jlom.master_upm.web.mvc_test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Pedido {

    private final static Logger logger = LoggerFactory.getLogger(Pedido.class.getName());

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long idPedido;
    private String nombrePedido;
    @OneToMany(cascade = CascadeType.ALL, targetEntity = Item.class)
    @ElementCollection(targetClass = Item.class)
    private List<Item> items;

    public Pedido() {
        logger.info("Pedido ctor ()");

        this.items = new ArrayList<>();
    }


    public long getIdPedido() {
        logger.info("Pedido getIdPedido (): " + this.idPedido);
        return idPedido;
    }
    public void setIdPedido(long idPedido) {
        logger.info("Pedido setIdPedido (" + idPedido +")");
        this.idPedido = idPedido;
    }

    public String getNombrePedido() {
        logger.info("Pedido getNombrePedido(): " + this.nombrePedido);
        return nombrePedido;
    }

    public void setNombrePedido(String nombrePedido) {
        logger.info("Pedido setNombrePedido (" + nombrePedido +")");
        this.nombrePedido = nombrePedido;
    }


    public List<Item> getItems() {
        logger.info("Pedido getItems(): " + this.items);
        return items;
    }

    public void setItems(List<Item> items) {
        logger.info("Pedido setItems (" + items +")");
        this.items = items;
    }

    @Override
    public String toString() {
        return "Pedido{" +
                "idPedido=" + idPedido +
                ", nombrePedido='" + nombrePedido + '\'' +
                ", items=" + items +
                '}';
    }
}
