package org.jlom.master_upm.web.mvc_test;


import org.jlom.utils.StreamUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
public class SimpleController {

    private final static Logger logger = LoggerFactory.getLogger(SimpleController.class.getName());

    private final PedidoRepository repository;

    @Autowired
    public SimpleController(PedidoRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/")
    public String index(Model model) {

        logger.info("index: model->" + model);
        Iterable<Pedido> pedidos = repository.findAll();
        model.addAttribute("pedidos", pedidos);

        logger.info("index: rendering (index)_model->" + model);
        return "index";
    }

    @GetMapping("/detalle_pedido/{idPedido}")
    public String detallePedido(@PathVariable Long idPedido, Model model) {

        logger.info("detallePedido: id:"+ idPedido
                + " - model:" + model
        );

        Optional<Pedido> byId = repository.findById(idPedido);
        model.addAttribute("pedido", byId.get());

        logger.info("detallePedido: rendering detalles_pedido id:"+ idPedido
                + " - model:" + model
        );

        return "detalles_pedido";
    }

    @PostMapping("editar_pedido")
    public String editarPedido(@RequestParam("idPedido") Long idPedido,
                               Model model) {

        logger.info("editar_pedido: id:"+ idPedido
                + " - model:" + model
        );

        Optional<Pedido> byId = repository.findById(idPedido);
        model.addAttribute("pedido", byId.get());

        logger.info("editar_pedido: rendering detalles_pedido id:"+ idPedido
                + " - model:" + model
        );

        return "edicion_pedido";
    }


    @PostMapping("pedidoEditado")
    public String pedidoEditado(Pedido pedido,
                                @RequestParam(value = "idItem", required = false) ArrayList<Long> idItems,
                                @RequestParam(value = "nombreItem", required = false) ArrayList<String> nombreItems,
                                @RequestParam(value = "striked_rel", required = false) ArrayList<Boolean> isStrikedItems,
                                Model model,
                                BindingResult result) {

        if (result.hasErrors()) {
                return "error";
        }

        logger.info("pedidoEditado: pedido:" + pedido
                + " - idItem:" + idItems
                + " - nombreItems:" + nombreItems
                + " - isStrikedItems:" + isStrikedItems
                + " - model:" + model
        );

        if ( (null != idItems) && (null != nombreItems) ) {
            assert (idItems.size() == nombreItems.size());
            assert (isStrikedItems.size() == nombreItems.size());

            List<Item> items = new ArrayList<>(idItems.size());

            for (int idx = 0; idx < nombreItems.size(); idx++) {
                if (idItems.get(idx)!= null && !nombreItems.get(idx).isEmpty()) {
                    Item item = new Item();
                    item.setIdItem(idItems.get(idx));
                    item.setNombreItem(nombreItems.get(idx));
                    item.setStriked(isStrikedItems.get(idx));
                    items.add(item);
                }
            }

            logger.info("pedidoEditado: pedido:" + pedido
                    + " - items:" + items
            );
            pedido.setItems(items);
        }

        logger.info("pedidoEditado: pedido:" + pedido);
        repository.save(pedido);

        logger.info("pedidoEditado: redirecting ... pedido:" + pedido
                + " - model:" + model
        );

        return "redirect:/detalle_pedido/" + pedido.getIdPedido();
    }

    @PostMapping("pedido_anadido")
    public String pedidoAnadido(@RequestParam(value = "nombrePedido") String nombrePedido,
                                @RequestParam(value = "nombreItem", required = false) ArrayList<String> nombreItems,
                                Model model){

        Pedido pedido = new Pedido();
        pedido.setNombrePedido(nombrePedido);

        logger.info("pedidoAnadido: pedido:" + pedido
                + " - nombreItems:" + nombreItems
                + " - model:" + model
        );

        if ( null != nombreItems)  {
            List<Item> items = new ArrayList<>(nombreItems.size());

            for (int idx = 0; idx < nombreItems.size(); idx++) {
                if (! nombreItems.get(idx).isEmpty()) {
                    Item item = new Item();
                    item.setNombreItem(nombreItems.get(idx));
                    items.add(item);
                }
            }

            logger.info("pedidoAnadido: pedido:" + pedido
                    + " - items:" + items
            );
            pedido.setItems(items);
        }

        logger.info("pedidoAnadido: pedido:" + pedido);
        repository.save(pedido);

        logger.info("pedidoAnadido: redirecting ... pedido:" + pedido
                + " - model:" + model
        );

        return "redirect:/";
    }

    @PostMapping("borrar_pedido")
    public String borrarPedido(@RequestParam("idPedido") Long idPedido) {

        logger.warn("Entering borrarPedido:" + idPedido);

        Pedido pedido = new Pedido();
        pedido.setIdPedido(idPedido);

        repository.delete(pedido);

        return "redirect:/";
    }
}
