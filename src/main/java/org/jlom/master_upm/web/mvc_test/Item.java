package org.jlom.master_upm.web.mvc_test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
public class Item {

    private long idItem;
    @NotNull
    @NotBlank
    private String nombreItem;

    private boolean striked;


    private final static Logger logger = LoggerFactory.getLogger(Item.class.getName());

    public Item() {
        this.striked = false;
        logger.info("Item constructor ()");
    }

    public String getNombreItem() {
        logger.info("Item getNombreItem (): " + this.nombreItem);
        return nombreItem;
    }

    public void setNombreItem(String nombreItem) {
        logger.info("Item setNombreItem (" + nombreItem + ")");
        this.nombreItem = nombreItem;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public long getIdItem() {
        return idItem;
    }

    public void setIdItem(long idItem) {
        this.idItem = idItem;
    }

    public boolean getStriked() {
        return striked;
    }

    public void setStriked(boolean striked) {
        this.striked = striked;
    }

    @Override
    public String toString() {
        return "Item{" +
                "idItem=" + idItem +
                ", nombreItem='" + nombreItem + '\'' +
                ", striked=" + striked +
                '}';
    }
}
