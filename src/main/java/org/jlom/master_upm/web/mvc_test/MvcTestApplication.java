package org.jlom.master_upm.web.mvc_test;

import ch.qos.logback.classic.Level;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.Arrays;

@SpringBootApplication
public class MvcTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(MvcTestApplication.class, args);
	}

	private static final Logger logger = LoggerFactory.getLogger(MvcTestApplication.class.getName());

	@Bean
	public CommandLineRunner populate(PedidoRepository repository) {
        return (args -> {

            ((ch.qos.logback.classic.Logger)LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME)).setLevel(Level.INFO);

            MvcTestApplication.logger.info("Populating database");


            Pedido pedido = new Pedido();
            pedido.setNombrePedido("pedido01");


            Item itemA = new Item();
            itemA.setNombreItem("itemA");

            Item itemB= new Item();
            itemB.setNombreItem("itemB");

            Item itemC= new Item();
            itemC.setNombreItem("itemC");

            pedido.setItems(Arrays.asList(itemA,itemB,itemC));

            repository.save(pedido);

            pedido  = new Pedido();
            pedido.setNombrePedido("pedido02");

            Item itemD= new Item();
            itemD.setNombreItem("itemD");

            pedido.setItems(Arrays.asList(itemD));
            repository.save(pedido);


            MvcTestApplication.logger.info("End Populating database");
        });
    }
}
