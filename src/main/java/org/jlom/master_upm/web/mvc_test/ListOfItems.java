package org.jlom.master_upm.web.mvc_test;

import java.util.List;

public class ListOfItems {

    private List<Item> items;

    public ListOfItems() {
    }


    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }


    @Override
    public String toString() {
        return "ListOfItems{" +
                "items=" + items +
                '}';
    }
}
