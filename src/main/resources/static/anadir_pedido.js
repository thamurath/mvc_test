$(document).ready(function(){
    document.getElementById("nuevoItem").addEventListener("click", cloning);
    document.getElementById("deleteBtn").addEventListener("click", onClickDeleteElement);
    checkSubmit();
    enableDelete();
});

function checkSubmit() {
    var $submits = $("button[type=submit]");
    var $inputs = $("input[type=text]:not(:hidden)");

    function isNoneEmpty() {

        var emptyOnes = $inputs.filter(function() {
            return !$.trim(this.value);
        });
        console.log(emptyOnes.length);
        return emptyOnes.length === 0;
    }

    function updateSubmit() {
        $submits.prop("disabled", !isNoneEmpty());
    }

    updateSubmit();

    $inputs.on("keyup blur", updateSubmit);
}



function enableDelete() {
    console.log("enableDelete");

    var $items=$("div[id=oneItem]:not(:hidden)");
    console.log("items:" + $items.length);

    var $deleteButtons = $("div[id=oneItem] button[id=deleteBtn]");

    if ($items.length > 1) {
        console.log("enableDelete:mas de uno --> visibles");
        $deleteButtons.prop('hidden', null);
    } else {
        console.log("enableDelete:menos de dos --> invisibles");
        $deleteButtons.prop('hidden', true);
    }

    $deleteButtons.on("click", onClickDeleteElement);
}

function cloning() {
    console.log("cloning");
    var container = document.getElementById('items');
    var clone = document.getElementById('2clone').cloneNode(true);
    clone.removeAttribute('hidden');
    container.appendChild(clone);
    checkSubmit();
    enableDelete();
}

function onClickDeleteElement() {
    console.log("onClickDeleteButton");

    // Esta es una de las peores maneras que puede haber para esto porque es
    // terriblemente dependiente de la estructura concreta del html...
    // pero no soy capaz de encontrar la manera de buscar desde un elemento
    // del DOM hacia arriba un parent que tenga una determinada caracteristica...
    // que seria una manera mas elegante de hacer esto.
    var $toDelete = this.parentNode.parentNode.parentNode;
    $toDelete.parentNode.removeChild($toDelete);
    enableDelete();
}